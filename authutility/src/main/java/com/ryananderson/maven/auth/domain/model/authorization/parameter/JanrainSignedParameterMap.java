package com.ryananderson.maven.auth.domain.model.authorization.parameter;

import com.ryananderson.maven.auth.domain.model.authorization.Scheme;

import java.util.HashMap;

public class JanrainSignedParameterMap extends ParameterMap {
    private String _headerDate;
    private String _clientIdentifier;
    private String _clientSecret;
    private String _requestPath;

    private HashMap<String, String> _requestParameters;

    public JanrainSignedParameterMap() {
        this(Scheme.SIGNATURE, new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ").format(new java.util.Date()), "clientidentifier", "clientsecret", "/endpointpath", new HashMap<String, String>());
    }

    public JanrainSignedParameterMap(Scheme scheme, String headerDate, String clientIdentifier, String clientSecret, String requestPath, HashMap<String, String> requestParameters) {
        super(scheme);

        _headerDate = headerDate;
        _clientIdentifier = clientIdentifier;
        _clientSecret = clientSecret;
        _requestPath = requestPath;
        _requestParameters = requestParameters;
    }

    public String getHeaderDate() {
        return _headerDate;
    }

    public void setHeaderDate(String headerDate) {
        _headerDate = headerDate;
    }

    public String getClientIdentifier() {
        return _clientIdentifier;
    }

    public void setClientIdentifier(String clientIdentifier) {
        _clientIdentifier = clientIdentifier;
    }

    public String getClientSecret() {
        return _clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        _clientSecret = clientSecret;
    }

    public String getRequestPath() {
        return _requestPath;
    }

    public void setRequestPath(String requestPath) {
        _requestPath = requestPath;
    }

    public void setRequestParameters(HashMap<String, String> requestParameters) {
        _requestParameters = requestParameters;
    }

    public HashMap<String, String> getRequestParameters() {
        return _requestParameters;
    }
}
