package com.ryananderson.maven.auth.domain.model.authorization;

public enum Algorithm {
    BASIC("HTTP Basic"),
    JANRAIN_SIGNED("Janrain Signed"),
    NONE("None"),
    OAUTH1A("OAuth 1.0a"),
    OAUTH2("OAuth 2.0");

    private String _description;

    private Algorithm(String description)
    {
        _description = description;
    }

    public String getDescription()
    {
        return _description;
    }
}
