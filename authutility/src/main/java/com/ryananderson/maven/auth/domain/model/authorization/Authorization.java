package com.ryananderson.maven.auth.domain.model.authorization;

import com.ryananderson.maven.auth.domain.model.authorization.parameter.HttpBasicParameterMap;
import com.ryananderson.maven.auth.domain.model.authorization.parameter.JanrainSignedParameterMap;
import com.ryananderson.maven.auth.domain.model.authorization.parameter.ParameterMap;
import com.ryananderson.maven.auth.utility.AuthorizationUtility;

public class Authorization {
    private Algorithm _algorithm;
    private ParameterMap _parameterMap;

    public Authorization(ParameterMap parameterMap)
    {
        _parameterMap = parameterMap;
    }

    public String getValue()
    {
        if(!(_algorithm == Algorithm.BASIC && _parameterMap instanceof HttpBasicParameterMap)
            && !(_algorithm == Algorithm.JANRAIN_SIGNED && _parameterMap instanceof JanrainSignedParameterMap))
        return "";

        return AuthorizationUtility.generateAuthorizationHeader(_parameterMap);
    }

    public Algorithm getAlgorithm()
    {
        return _algorithm;
    }

    public void setAlgorithm(Algorithm algorithm)
    {
        _algorithm = algorithm;
    }

    public ParameterMap getParameterMap()
    {
        return _parameterMap;
    }

    public void setParameterMap(ParameterMap parameterMap)
    {
        _parameterMap = parameterMap;
    }
}
