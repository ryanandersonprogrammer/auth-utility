package com.ryananderson.maven.auth.domain.model.authorization;

public enum Scheme {
    SIGNATURE("Signature"),
    OAUTH("OAuth"),
    BEARER("Bearer"),
    BASIC("Basic");

    private String _schemeName;

    private Scheme(String schemeName)
    {
        _schemeName = schemeName;
    }

    public String getSchemeName()
    {
        return _schemeName;
    }
}
