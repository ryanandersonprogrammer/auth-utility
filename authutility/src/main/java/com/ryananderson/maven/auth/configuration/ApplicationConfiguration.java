package com.ryananderson.maven.auth.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.ryananderson"})
class ApplicationConfiguration implements WebMvcConfigurer {
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer defaultServletHandlerConfigurer)
    {
        defaultServletHandlerConfigurer.enable();
    }

    public void addResourceHandlers(ResourceHandlerRegistry resourceHandlerRegistry)
    {
        resourceHandlerRegistry
                .addResourceHandler("/resources/**")
                .addResourceLocations("classpath:/static/")
                .setCacheControl(CacheControl.maxAge(3600, TimeUnit.SECONDS).cachePublic());
    }

    @Bean
    public InternalResourceViewResolver jspViewResolver()
    {
        return getResolver("/WEB-INF/view/", ".jsp", 0);
    }

    @Bean
    public InternalResourceViewResolver htmlViewResolver()
    {
        return getResolver("/WEB-INF/html/", ".html", 1);
    }

    private InternalResourceViewResolver getResolver(String prefix, String suffix, int order)
    {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();

        bean.setViewClass(JstlView.class);
        bean.setPrefix(prefix);
        bean.setSuffix(suffix);
        bean.setOrder(order);
        bean.setExposeContextBeansAsAttributes(true);

        return bean;
    }
}
