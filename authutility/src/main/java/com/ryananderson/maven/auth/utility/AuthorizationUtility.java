package com.ryananderson.maven.auth.utility;

import com.ryananderson.maven.auth.domain.model.authorization.Scheme;
import com.ryananderson.maven.auth.domain.model.authorization.parameter.HttpBasicParameterMap;
import com.ryananderson.maven.auth.domain.model.authorization.parameter.JanrainSignedParameterMap;
import com.ryananderson.maven.auth.domain.model.authorization.parameter.ParameterMap;

import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import java.util.*;
import java.util.stream.Collectors;

public class AuthorizationUtility {
    public static String generateAuthorizationHeader(ParameterMap parameterMap) {
        if (parameterMap instanceof HttpBasicParameterMap) {
            HttpBasicParameterMap httpBasicParameterMap;

            String encodedString;

            httpBasicParameterMap = (HttpBasicParameterMap) parameterMap;

            httpBasicParameterMap.setScheme(Scheme.BASIC);

            encodedString = Base64.encodeBase64String(String.format("%s:%s", httpBasicParameterMap.getClientIdentifier(), httpBasicParameterMap.getClientSecret()).getBytes());

            return String.format("%s %s", parameterMap.getScheme().getSchemeName(), encodedString);
        } else if (parameterMap instanceof JanrainSignedParameterMap) {
            JanrainSignedParameterMap janrainSignedParameterMap;

            String hash;
            String clientIdentifier;

            hash = clientIdentifier = "";

            janrainSignedParameterMap = (JanrainSignedParameterMap) parameterMap;

            janrainSignedParameterMap.setScheme(Scheme.SIGNATURE);

            if (janrainSignedParameterMap.getRequestParameters().size() > 0) {
                String stringToSign;
                String clientSecret;
                String sortedFormattedMap;

                Mac hmac;

                clientIdentifier = janrainSignedParameterMap.getClientIdentifier();
                clientSecret = janrainSignedParameterMap.getClientSecret();

                Map<String, String> sortedParameterMap = janrainSignedParameterMap
                        .getRequestParameters()
                        .entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByKey())
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (oldValue, newValue) -> oldValue, LinkedHashMap::new));

                sortedFormattedMap = sortedParameterMap
                        .entrySet()
                        .stream()
                        .map(x -> String.format("%s=%s", x.getKey(), x.getValue()))
                        .collect(Collectors.joining("\n"));

                if (clientSecret == null)
                    clientSecret = "";

                stringToSign = String.format("%s\n%s\n%s\n", janrainSignedParameterMap.getRequestPath(),
                        janrainSignedParameterMap.getHeaderDate(), sortedFormattedMap);

                hmac = HmacUtils.getInitializedMac("HmacSHA1", clientSecret.getBytes());

                hash = Base64.encodeBase64String(HmacUtils.updateHmac(hmac, stringToSign).doFinal());
            }

            return String.format("%s %s:%s", parameterMap.getScheme().getSchemeName(), clientIdentifier, hash);
        } else {
            return "";
        }
    }
}
