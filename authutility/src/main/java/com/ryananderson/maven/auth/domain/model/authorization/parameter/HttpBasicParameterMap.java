package com.ryananderson.maven.auth.domain.model.authorization.parameter;

import com.ryananderson.maven.auth.domain.model.authorization.Scheme;

public class HttpBasicParameterMap extends ParameterMap {
    private String _clientIdentifier;
    private String _clientSecret;

    public HttpBasicParameterMap() {
        this(Scheme.BASIC, "clientidentifier", "clientsecret");
    }

    public HttpBasicParameterMap(Scheme scheme, String clientIdentifier, String clientSecret) {
        super(scheme);

        _clientIdentifier = clientIdentifier;
        _clientSecret = clientSecret;
    }

    public String getClientIdentifier() {
        return _clientIdentifier;
    }

    public void setClientIdentifier(String clientIdentifier) {
        _clientIdentifier = clientIdentifier;
    }

    public String getClientSecret() {
        return _clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        _clientSecret = clientSecret;
    }
}
