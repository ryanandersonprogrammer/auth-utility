package com.ryananderson.maven.auth.domain.model.authorization.parameter;

import com.ryananderson.maven.auth.domain.model.authorization.Scheme;

public class ParameterMap {
    private Scheme _scheme;

    public ParameterMap(Scheme scheme)
    {
        _scheme = scheme;
    }

    public void setScheme(Scheme scheme)
    {
        _scheme = scheme;
    }

    public Scheme getScheme()
    {
        return _scheme;
    }
}
