package com.ryananderson.maven.auth.controller;

import com.ryananderson.maven.auth.domain.model.authorization.Algorithm;
import com.ryananderson.maven.auth.domain.model.authorization.Authorization;
import com.ryananderson.maven.auth.domain.model.authorization.Scheme;
import com.ryananderson.maven.auth.domain.model.authorization.parameter.HttpBasicParameterMap;
import com.ryananderson.maven.auth.domain.model.authorization.parameter.JanrainSignedParameterMap;
import com.ryananderson.maven.auth.domain.model.authorization.parameter.ParameterMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@SessionAttributes("authorization")
public class AuthorizationController {
    @GetMapping(value = {"/", "authorizationHome"})
    public ModelAndView showAlgorithmForm() {
        return new ModelAndView("authorizationHome","authorization",
                new Authorization(new HttpBasicParameterMap(Scheme.BASIC,"clientidentifier","clientsecret")));
    }

    @ModelAttribute("algorithms")
    public List<Algorithm> algorithms() {
        return Arrays.stream(Algorithm.values()).filter(x -> x != Algorithm.NONE).collect(Collectors.toList());
    }

    @RequestMapping(value = "/setArguments", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView showForm(@ModelAttribute("authorization") final Authorization authorization,
                                 final BindingResult result, final ModelMap model) {
        switch (authorization.getAlgorithm()) {
            case BASIC:
                return new ModelAndView("authorizationHttpBasicParameter", "httpBasicParameterMap", new HttpBasicParameterMap());
            case JANRAIN_SIGNED:
                return new ModelAndView("authorizationJanrainSignedParameter", "janrainSignedParameterMap", new JanrainSignedParameterMap());
            default:
                return new ModelAndView("error");
        }
    }

    @RequestMapping(value = "/generateHttpBasicHeader", method = RequestMethod.POST)
    public String submitHttpBasicArguments(@SessionAttribute("authorization") final Authorization authorization,
                                           @ModelAttribute("httpBasicParameterMap") final HttpBasicParameterMap httpBasicParameterMap,
                                           final BindingResult result, final ModelMap model) {
        if (result.hasErrors())
            return "error";

        return getAuthorizationView(authorization, httpBasicParameterMap, model);
    }

    @RequestMapping(value = "/generateJanrainSignedHeader", method = RequestMethod.POST)
    public String submitJanrainSignedArguments(@SessionAttribute("authorization") final Authorization authorization,
                                               @ModelAttribute("janrainSignedParameterMap") final JanrainSignedParameterMap janrainSignedParameterMap,
                                               @RequestParam("keys[]") final String[] keys, @RequestParam("values[]") final String[] values,
                                               final BindingResult result, final ModelMap model) {
        if (result.hasErrors())
            return "error";

        HashMap<String, String> requestParameters;

        requestParameters = janrainSignedParameterMap.getRequestParameters();

        for (int i = 0; i < keys.length; i++)
            requestParameters.put(keys[i], values[i]);

        return getAuthorizationView(authorization, janrainSignedParameterMap, model);
    }

    private String getAuthorizationView(Authorization authorization, ParameterMap parameterMap, ModelMap model) {
        authorization.setParameterMap(parameterMap);

        model.addAttribute("value", authorization.getValue());
        model.addAttribute("algorithm", authorization.getAlgorithm().getDescription());

        return "authorizationView";
    }
}
