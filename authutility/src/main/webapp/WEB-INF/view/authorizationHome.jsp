<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <link rel="stylesheet" href="<spring:url value="/resources/css/style.css" />"/>

    <title>Authorization Utility | Utility</title>
</head>
<body>
<div id="wrapper">
    <header>
        <h1 class="golden-header"><a href="${pageContext.request.contextPath}/">Auth Utility</a></h1>
        <h2 class="golden-header">v1.0.0</h2>
    </header>
    <main>
        <section>
            <h3 class="golden-header">Utility</h3>
            <hr>
            <article>
                <div class="form-section-wrapper">
                    <h4 class="golden-header"><u>Step 1:</u></h4>
                    <p>Use the form below to generate an authorization header.</p>
                    <hr>
                    <h5 class="golden-header">Form</h5>
                    <form:form method="POST"
                               action="${pageContext.request.contextPath}/setArguments"
                               class="form"
                               modelAttribute="authorization">
                        <fieldset>
                            <legend>Filters</legend>
                            <div id="algorithm-wrapper" class="form-group">
                                <form:label for="algorithm"
                                            path="algorithm"
                                            class="form-horizontal-label">Algorithm:</form:label>
                                <form:select id="algorithm" path="algorithm" class="form-input" onchange="autoSubmit()">
                                    <form:option value="NONE" label="Select an algorithm:"/>
                                    <form:options items="${algorithms}" itemLabel="description"/>
                                </form:select>
                            </div>
                        </fieldset>
                    </form:form>
                </div>
            </article>
        </section>
    </main>
    <footer>
        <div class="fine-print">
            <p>
                <small>
                    Copyright &copy; 2018 Ryan Anderson
                </small>
            </p>
        </div>
    </footer>
</div>
<script>
    function autoSubmit() {
        const form = document.querySelector("form");
        const list = document.querySelector("#algorithm");
        const selectedItem = list.options[list.selectedIndex];

        if (selectedItem.value === "NONE")
            return;

        form.submit();
    }
</script>
</body>
</html>