<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <link rel="stylesheet" href="<spring:url value="/resources/css/style.css" />"/>

    <title>Authorization Utility | Error</title>
</head>
<body>
<div id="wrapper">
    <header>
        <h1 class="golden-header"><a href="${pageContext.request.contextPath}/">Auth Utility</a></h1>
        <h2 class="golden-header">v1.0.0</h2>
    </header>
    <main>
        <section>
            <h3 class="golden-header">Error</h3>
            <hr>
            <article>
                <h4 class="golden-header">Form Processing Error</h4>
                <p>Click the button to return to the first step.</p>
                <hr>
                <h5 class="golden-header">Form</h5>
                <form id="error-form"
                      class="form">
                    <fieldset>
                        <legend>Filters</legend>
                        <div class="form-control">
                            <button id="error-cancel-button"
                                    type="button"
                                    class="form-button-right tertiary-button"
                                    onclick="cancel('authorizationHome')">Cancel
                            </button>
                        </div>
                    </fieldset>
                </form>
            </article>
        </section>
    </main>
    <footer>
        <div class="fine-print">
            <p>
                <small>
                    Copyright &copy; 2018 Ryan Anderson
                </small>
            </p>
        </div>
    </footer>
</div>
<script>
    function cancel(uri) {
        window.location = "${pageContext.request.contextPath}/" + uri;

        return false;
    }
</script>
</body>
</html>
