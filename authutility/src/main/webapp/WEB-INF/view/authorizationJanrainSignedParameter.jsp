<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <link rel="stylesheet" href="<spring:url value="/resources/css/style.css" />"/>

    <title>Authorization Utility | Janrain Signed Params</title>
</head>
<body>
<div id="wrapper">
    <header>
        <h1 class="golden-header"><a href="${pageContext.request.contextPath}/">Auth Utility</a></h1>
        <h2 class="golden-header">v1.0.0</h2>
    </header>
    <main>
        <section>
            <h3 class="golden-header">Janrain Signed Parameters</h3>
            <hr>
            <article>
                <h4 class="golden-header"><u>Step 2:</u></h4>
                <p>Set the arguments for the authorization parameters of the scheme, and submit the form.</p>
                <hr>
                <h5 class="golden-header">Form</h5>
                <form:form id="janrain-signed-parameter-form"
                           method="POST"
                           action="${pageContext.request.contextPath}/generateJanrainSignedHeader"
                           class="form"
                           modelAttribute="janrainSignedParameterMap">
                    <fieldset>
                        <legend>Filters</legend>
                        <div class="form-group">
                            <form:label for="request-path" path="requestPath"
                                        class="form-horizontal-label">Janrain API Request Path:</form:label>
                            <form:input id="request-path" path="requestPath"
                                        placeholder="enter endpoint" class="form-input"/>
                        </div>
                        <div class="form-group">
                            <label for="param-keys" class="form-horizontal-label">Param Keys:</label>
                            <input type="text" id="param-keys" name="keys[]" placeholder="enter key 1"
                                   class="form-input"/>
                            <label for="param-values" class="form-horizontal-label">Param Values:</label>
                            <input type="text" id="param-values" name="values[]" placeholder="enter value 1"
                                   class="form-input">
                        </div>
                        <div class="form-group">
                            <form:label for="timestamp" path="headerDate"
                                        class="form-horizontal-label">Date+Time:</form:label>
                            <form:input type="text" id="timestamp" path="headerDate" class="form-input"
                                        placeholder="enter timestamp"/>
                        </div>
                        <div class="form-group">
                            <form:label for="janrain-signed-client-identifier" path="clientIdentifier"
                                        class="form-horizontal-label">Client Identifier:</form:label>
                            <form:input type="text" id="janrain-signed-client-identifier"
                                        path="clientIdentifier" class="form-input"
                                        placeholder="enter client identifier"/>
                        </div>
                        <div class="form-group">
                            <form:label for="janrain-signed-client-secret" path="clientSecret"
                                        class="form-horizontal-label">Client Secret:</form:label>
                            <form:input type="text" id="janrain-signed-client-secret"
                                        path="clientSecret"
                                        class="form-input"
                                        placeholder="enter client secret"/>
                        </div>
                        <div class="form-control">
                            <button id="janrain-signed-cancel-button"
                                         type="button"
                                         class="form-button-right tertiary-button"
                                         onclick="cancel('authorizationHome')">Cancel</button>
                            <button id="janrain-signed-trim-button"
                                         type="button"
                                         class="form-button-right secondary-button"
                                         onclick="trim()">Trim</button>
                            <button id="janrain-signed-clear-button"
                                         type="button"
                                         class="form-button-right secondary-button"
                                         onclick="clearForm()">Clear</button>
                            <button id="janrain-signed-reset-button"
                                         type="reset"
                                         class="form-button-right secondary-button">Reset</button>
                            <button id="janrain-signed-remove-button"
                                         type="button"
                                         class="form-button-right primary-button"
                                         onclick="remove()">Remove</button>
                            <button id="janrain-signed-add-button"
                                         type="button"
                                         class="form-button-right primary-button"
                                         onclick="add()">Add</button>
                            <form:button id="janrain-signed-submit-button"
                                         class="form-button-right primary-button">Submit</form:button>
                        </div>
                    </fieldset>
                </form:form>
            </article>
        </section>
    </main>
    <footer>
        <div class="fine-print">
            <p>
                <small>
                    Copyright &copy; 2018 Ryan Anderson
                </small>
            </p>
        </div>
    </footer>
</div>
<script>
    function cancel(uri) {
        window.location = "${pageContext.request.contextPath}/" + uri;

        return false;
    }

    function add() {
        const form = document.querySelector("form");
        const keyInputs = form.querySelectorAll("input[name='keys[]']");
        const valueInputs = form.querySelectorAll("input[name='values[]']");
        const keyInputCount = keyInputs.length;
        const inputKey = document.createElement("input");
        const inputValue = document.createElement("input");

        inputKey.setAttribute("type", "text");
        inputKey.setAttribute("id", "param-keys-" + (keyInputCount + 1));
        inputKey.setAttribute("name", "keys[]");
        inputKey.setAttribute("placeholder", "enter key " + (keyInputCount + 1));
        inputKey.setAttribute("class", "form-input");
        inputValue.setAttribute("type", "text");
        inputValue.setAttribute("id", "param-values-" + (keyInputCount + 1));
        inputValue.setAttribute("name", "values[]");
        inputValue.setAttribute("placeholder", "enter value " + (keyInputCount + 1));
        inputValue.setAttribute("class", "form-input");

        keyInputs[keyInputCount - 1].insertAdjacentElement('afterend', inputKey);
        valueInputs[keyInputCount - 1].insertAdjacentElement('afterend', inputValue);
    }

    function remove() {
        const form = document.querySelector("form");
        const keyInputs = [...form.querySelectorAll("input[name='keys[]']")];
        const keyInputCount = keyInputs.length;

        if (keyInputCount > 1) {
            const valueInputs = [...form.querySelectorAll("input[name='values[]']")];

            keyInputs[keyInputCount - 1].remove();
            valueInputs[keyInputCount - 1].remove();
        }
    }

    function trim() {
        const form = document.querySelector("form");
        const valueInputs = form.querySelectorAll("input[type='text']");
        const valueInputCount = valueInputs.length;

        for (let i = 0; i < valueInputCount; i++) {
            valueInputs[i].value = valueInputs[i].value.trim();
        }
    }

    function clearForm() {
        const form = document.querySelector("form");
        const valueInputs = form.querySelectorAll("input[type='text']");
        const valueInputCount = valueInputs.length;

        for (let i = 0; i < valueInputCount; i++) {
            valueInputs[i].value = "";
        }
    }
</script>
</body>
</html>