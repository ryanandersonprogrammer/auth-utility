<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <link rel="stylesheet" href="<spring:url value="/resources/css/style.css" />"/>

    <title>Authorization Utility | Header</title>
</head>
<body>
<div id="wrapper">
    <header>
        <h1 class="golden-header"><a href="${pageContext.request.contextPath}/">Auth Utility</a></h1>
        <h2 class="golden-header">v1.0.0</h2>
    </header>
    <main>
        <section>
            <h3 class="golden-header">Header</h3>
            <hr>
            <article>
                <h4 class="golden-header"><u>Step 3:</u></h4>
                <p>Copy the authorization header.</p>
                <hr>
                <h5 class="golden-header">Form</h5>
                <form id="header-copy-form"
                      class="form">
                    <fieldset>
                        <legend>Filters</legend>
                        <div class="form-group">
                            <label for="algorithm" class="form-horizontal-label">Algorithm:</label>
                            <input type="text" id="algorithm" name="algorithm" value="${algorithm}" class="form-input"
                                   readonly>
                        </div>
                        <div class="form-group">
                            <label for="value" class="form-horizontal-label">Header Value:</label>
                            <input type="text" id="value" name="value" value="${value}" class="form-input" readonly/>
                        </div>
                        <div class="form-control">
                            <button id="header-copy-cancel-button"
                                    type="button"
                                    class="form-button-right tertiary-button"
                                    onclick="cancel('authorizationHome')">Cancel
                            </button>
                            <button id="header-copy-submit-button"
                                    type="submit"
                                    class="form-button-right primary-button">Submit
                            </button>
                        </div>
                    </fieldset>
                </form>
            </article>
        </section>
    </main>
    <footer>
        <div class="fine-print">
            <p>
                <small>
                    Copyright &copy; 2018 Ryan Anderson
                </small>
            </p>
        </div>
    </footer>
</div>
<script>
    function cancel(uri) {
        window.location = "${pageContext.request.contextPath}/" + uri;

        return false;
    }

    document.querySelector("form").addEventListener("submit", function (e) {
        e.preventDefault();

        const form = document.querySelector("form");
        const headerValue = form.querySelector("#value");

        headerValue.select();

        document.execCommand("copy");

        alert("The following text was copied to the clipboard: " + headerValue.value);

        return false;
    });
</script>
</body>
</html>