<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <link rel="stylesheet" href="<spring:url value="/resources/css/style.css" />"/>

    <title>Authorization Utility | HTTP Basic Params</title>
</head>
<body>
<div id="wrapper">
    <header>
        <h1 class="golden-header"><a href="${pageContext.request.contextPath}/">Auth Utility</a></h1>
        <h2 class="golden-header">v1.0.0</h2>
    </header>
    <main>
        <section>
            <h3 class="golden-header">HTTP "Basic" Parameters</h3>
            <hr>
            <article>
                <h4 class="golden-header"><u>Step 2:</u></h4>
                <p>Set the arguments for the authorization parameters of the scheme, and submit the form.</p>
                <hr>
                <h5 class="golden-header">Form</h5>
                <form:form id="basic-parameter-form"
                           method="POST"
                           action="${pageContext.request.contextPath}/generateHttpBasicHeader"
                           class="form"
                           modelAttribute="httpBasicParameterMap">
                    <fieldset>
                        <legend>Filters</legend>
                        <div class="form-group">
                            <form:label for="basic-client-identifier"
                                        path="clientIdentifier"
                                        class="form-horizontal-label">Client Identifier:</form:label>
                            <form:input type="text"
                                        id="basic-client-identifier"
                                        path="clientIdentifier"
                                        class="form-input"
                                        placeholder="enter client identifier"/>
                        </div>
                        <div class="form-group">
                            <form:label for="basic-client-secret"
                                        path="clientSecret"
                                        class="form-horizontal-label">Client Secret:</form:label>
                            <form:input type="text" id="basic-client-secret"
                                        path="clientSecret"
                                        class="form-input"
                                        placeholder="enter client secret"/>
                        </div>
                        <div class="form-control">
                            <button id="basic-cancel-button"
                                         type="button"
                                         class="form-button-right tertiary-button"
                                         onclick="cancel('authorizationHome')">Cancel</button>
                            <button id="basic-trim-button"
                                         type="button"
                                         class="form-button-right secondary-button"
                                         onclick="trim()">Trim</button>
                            <button id="basic-clear-button"
                                         type="button"
                                         class="form-button-right secondary-button"
                                         onclick="clearForm()">Clear</button>
                            <button id="basic-reset-button"
                                         type="reset"
                                         class="form-button-right secondary-button">Reset</button>
                            <form:button id="basic-submit-button"
                                         class="form-button-right primary-button">Submit</form:button>
                        </div>
                    </fieldset>
                </form:form>
            </article>
        </section>
    </main>
    <footer>
        <div class="fine-print">
            <p>
                <small>
                    Copyright &copy; 2018 Ryan Anderson
                </small>
            </p>
        </div>
    </footer>
</div>
<script>
    function cancel(uri) {
        window.location = "${pageContext.request.contextPath}/" + uri;

        return false;
    }

    function trim() {
        const form = document.querySelector("form");
        const valueInputs = form.querySelectorAll("input[type='text']");
        const valueInputCount = valueInputs.length;

        for (let i = 0; i < valueInputCount; i++) {
            valueInputs[i].value = valueInputs[i].value.trim();
        }
    }

    function clearForm() {
        const form = document.querySelector("form");
        const valueInputs = form.querySelectorAll("input[type='text']");
        const valueInputCount = valueInputs.length;

        for (let i = 0; i < valueInputCount; i++) {
            valueInputs[i].value = "";
        }
    }
</script>
</body>
</html>