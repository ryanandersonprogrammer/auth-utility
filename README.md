﻿# Auth Utility

## About



### Author

Ryan E. Anderson

---

### Description

This utility can be used to generate authorization headers. The following schemes can be used to generate a header: HTTP "Basic" and Janrain Signed.

---

### Version

1.0.0

---

### License

MIT